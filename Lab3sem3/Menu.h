#include <iostream>
#include <stdlib.h>
#include "Train.h"


int InputCheck() {
    int number = 0;
    std::cin >> number;
    while (std::cin.fail()) {
        std::cin.clear();     // ����� ��������� �������� ����� ���� ����� ���� ������ �������� 
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // � ������� ��� ����������� � ��� ������
        std::cout << "Incorrect! Repeat Input:";
        std::cin >> number;
    }
    return number;
}

void run_menu()
{
    int choice, capacity, fill, adress;
    Train train;

    train.Fill_Carriage(0, 1);
    train.Fill_Carriage(1, 2);
    train.Fill_Carriage(2, 3);
    train.Fill_Carriage(3, 4);


    while (1) {

        printf(" 1.Create carriage\n 2.Input Carriages\n 3.Fill seats in carriage\n 4.Uncoupling\n 5.Prople in train \n 6.People in total \n 0. EXIT");
        printf("\n");
        choice = InputCheck();

        if (choice == 1) {
            std::cout << "Carriage fill: ";
            fill = InputCheck();
            std::cout << "Carriage capacity: ";
            capacity = InputCheck();
            if (train.Create_Carriage(fill, capacity) == EXIT_FAILURE) {
                std::cout << "Train is full!\n";
                continue;
            }
            std::cout << "Sucsessfully added!\n";

        }

        if (choice == 2) {
            for (int i = 0; i < train.carriage_amount; i++) {
                std::cout << "[" << i << "]:\n";
                std::cout << "  Carriage fill: ";
                fill = InputCheck();
                std::cout << "  Carriage capacity: ";
                capacity = InputCheck();
                train.Create_Carriage(fill, capacity);
            }
            std::cout <<"\n";
    
        }

        if (choice == 3) {
            std::cout << "Carriage adress: ";
            adress = InputCheck();
            std::cout << "\n Seats amount: ";
            fill = InputCheck();
            train.Fill_Carriage(adress, fill);
        }

        if (choice == 4) {
            std::cout << "Carriages amount: \n";
            adress = InputCheck();
            int* uncoupling_adresses = new int [adress];
            for (int i = 0; i < adress; i++) {
                std::cout << "adress: \n";
                uncoupling_adresses[i] = InputCheck();
            }
            Train new_train = train.Uncoupling_Train(adress, uncoupling_adresses);
            new_train.Print_Carriage_Fill();
            delete  uncoupling_adresses;
        }

        if (choice == 5) {
            train.Print_Carriage_Fill();
        }

        if (choice == 6) {
            std::cout << "Total amount of people:" << train.People_in_Train() << "\n";
        }
        if (choice == 0) {
            break;
        }
    }
}