#include <iostream>
#include <stdlib.h>
#define SIZE_TRAIN 10
#define SIZE_CARRIAGE 50

typedef struct Carriage {                                         // ����� 
	int capacity = SIZE_CARRIAGE;                                 // ���� ����� ���� 
	int fill = 0;												  // ������ ���� 
	
	Carriage() = default;
	Carriage(int size_carr_new, int fill_) : capacity(size_carr_new), fill(fill_) {};

}Carriage;

class Train {													  // �����

public:

	int carriage_amount = SIZE_TRAIN;							 //����� �������
	Carriage carriage[SIZE_TRAIN];


private:
	void Empty_Carriage(int adress);

public:

	Train() = default;                                          // ������������� ��� �������						 

	void Init_Carriages();										 // ������������� ���� ������� 

	int Create_Carriage(int fill, int size_carriage);                              // ���������������� ���� �� ��������� ������ �������

	void Print_Carriage_Fill();	                       // ����������� >> <<		    // ����� ��������� ������� � �����

	void Fill_Carriage(unsigned int adress, unsigned int seats_amount);            // ��������� ������ � ������

	unsigned int Get_Fill_Carriage(unsigned int adress);

	Train Uncoupling_Train(int amount_uncoupling_carriages, int* carriages_adress); // �������� (����� ������������, ������)

	int People_in_Train();										 // ����� ����� ����� � ������
};