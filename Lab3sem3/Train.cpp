#include "Train.h"

void Train::Init_Carriages() {
	int size = 0;
	int fill = 0;
	for (int i = 0; i < carriage_amount; i++) {
		if (carriage[i].fill != 0) {
			continue;
		}
		std::cout << i << ". " << "carriage size: ";
		std::cin >> size;
		std::cout << "fill: ";
		carriage[i].capacity = size;
		carriage[i].fill = fill;
		std::cout << "\n";
	}
}

void Train::Print_Carriage_Fill() {
	for (int i = 0; i < carriage_amount; i++) {
		std::cout << "[" << i << "]:" << carriage[i].fill << " out of " << carriage[i].capacity << " are busy.\n";
	}
	std::cout << "\n";
}

void Train::Fill_Carriage(unsigned  int adress, unsigned int seats_amount) {
	if (adress > carriage_amount) {
		return;
	}
	if (carriage[adress].fill + seats_amount > carriage[adress].capacity) {
		carriage[adress].fill = carriage[adress].capacity;
	}
	else {
		carriage[adress].fill += seats_amount;
	}
}

void Train::Empty_Carriage(int adress) {
	if (adress > carriage_amount) {
		return;
	}
	carriage[adress].fill = 0;
}

Train Train::Uncoupling_Train(int amount_uncoupling_carriages, int* carriages_adress) {
	Train new_train;

	for (int i = 0; i < amount_uncoupling_carriages; i++) {
		new_train.Create_Carriage(carriage[carriages_adress[i]].fill, carriage[carriages_adress[i]].capacity);
		Empty_Carriage(carriages_adress[i]);
	}
	
	return new_train;
}


int Train::People_in_Train() {
	int count = 0;
	for (int i = 0; i < carriage_amount; i++) {
		count += carriage[i].fill;
	}
	return count;
}

int Train::Create_Carriage(int fill, int size_carriage) {
	for (int i = 0; i < carriage_amount; i++) {
		if (carriage[i].fill == 0) {
			carriage[i].capacity = size_carriage;
			carriage[i].fill = fill;
			if (fill > size_carriage) {
				carriage[i].fill = size_carriage;
			}
			return EXIT_SUCCESS;
		}
	}
	return EXIT_FAILURE;
}

unsigned int Train::Get_Fill_Carriage(unsigned int adress) {
	return carriage[adress].fill;
}
