#include "Train.h"




Train::Train():_body({{0,0}}) {}

std::istream& operator>>(std::istream& stream, Train& train) {
	size_t size;
	stream >> size;
	train._body.clear();
	train.allpeople = 0;
	train._body.reserve(size);
	for (size_t i = 0; i < size; i++) {
		size_t capacity;
		stream >> capacity;
		size_t fill;
		stream >> fill;
		if (fill > capacity) {
			throw TooManyPeopleError();
		}
		train._body.push_back({ capacity, fill });
		train.allpeople += fill;
	}
	return stream;
}

void Train::Fill_Carriage(size_t adress, size_t seats_amount) {
	if (adress >= _body.size()) {
		throw IndexError();
	}
	if (_body[adress].fill + seats_amount > _body[adress].capacity) {
		throw TooManyPeopleError();
	}
	_body[adress].fill += seats_amount;
	allpeople += seats_amount;
}

Train& Train::operator+=(const Train& other) {
	_body.reserve(_body.size() + other._body.size());
	for (size_t i = 0; i < other._body.size(); i++) {
		_body.push_back(other._body[i]);
	}                                                                 
	return *this;
	allpeople += other.allpeople;
}


Train operator+(const Train& first, const Train& second) {
	return Train(first) += second;
}

std::ostream& operator<<(std::ostream& stream, const Train& train) {
	for (size_t i = 0; i < train._body.size(); i++) {
		stream << "[" << i << "]:" << train._body[i].fill << " \ " << train._body[i].capacity << "\n";
	}
	return stream;
}

Train operator*(const Train& first, size_t number) {
	Train base_train = first;
	for (size_t i = 0; i < number; i++) {
		Train(first) += base_train;
	}
	return first;
}

size_t Train::People_in_Train() {
	return allpeople;
}

size_t Train::Train_Size() {
	return _body.size();
}

void Train::Create_Carriage(size_t fill, size_t size_carriage) {
	if (fill > size_carriage) {
		throw TooManyPeopleError();
	}
	Carriage new_carriage = {  size_carriage, fill };
	_body.push_back(new_carriage);
	allpeople += fill;
}

size_t Train::Carriage_Fill(size_t adress) {
	if (adress >= _body.size()) {
		throw IndexError();
	}
	return _body[adress].fill;
}

void Train::Delete_Carriage(int adress) {
	if (adress > _body.size() - 1 || adress < 0  ) {
		throw DeleteError();
	}
	_body.erase(_body.begin() + adress);

}

Train Train::Uncoupling_Train(size_t amount_uncoupling_carriages, size_t* carriages_adress) {
	Train new_train;
	for (int i = 0; i < amount_uncoupling_carriages; i++) {
		new_train.Create_Carriage(_body[carriages_adress[i]].fill, _body[carriages_adress[i]].capacity);
		new_train.allpeople += _body[carriages_adress[i]].fill;
	}
	for (int i = amount_uncoupling_carriages; i > 0; i--) {
		allpeople -= _body[carriages_adress[i - 1]].fill;
		Delete_Carriage(carriages_adress[i-1]);
	}
	new_train.Delete_Carriage(0);
	return new_train;
}