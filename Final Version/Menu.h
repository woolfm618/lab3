#include <iostream>
#include <stdlib.h>
#include "Train.h"


int InputCheck() {
    int number = 0;
    std::cin >> number;
    while (std::cin.fail()) {
        std::cin.clear();     // ����� ��������� �������� ����� ���� ����� ���� ������ �������� 
        std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n'); // � ������� ��� ����������� � ��� ������
        std::cout << "Incorrect! Repeat Input:";
        std::cin >> number;
    }
    return number;
}

void run_menu()
{
    int choice, capacity, fill, adress;
    Train train;

    while (1) {

        printf(" 1.Add carriage\n 2.Input train\n 3.Fill seats in carriage\n 4.Uncoupling\n 5.Show train \n 6.People in total \n 7.People in cariage\n 0. EXIT");
        printf("\n");
        choice = InputCheck();

        if (choice == 1) {
            std::cout << "Carriage fill:\n";
            fill = InputCheck();
            std::cout << "Carriage capacity:\n";
            capacity = InputCheck();
            if (fill >capacity) {
                std::cout << "Incorrect input! No such carriage!\n";
                return;
            }
            train.Create_Carriage(fill, capacity);
            std::cout << "Sucsessfully added!\n";

        }

        if (choice == 2) {
            std::cout << "Train size:\n";
            std::cin >> train;
        }

        if (choice == 3) {
            std::cout << "Carriage adress: ";
            adress = InputCheck();
            std::cout << "\n Seats amount: ";
            fill = InputCheck();
            if (adress > train.Train_Size()) {
                std::cout << "Incorrect input! No such carriage!\n";
                return;
            }
            train.Fill_Carriage(adress, fill);
        }

        if (choice == 4) {
            std::cout << "Carriages amount: \n";
            adress = InputCheck();
            size_t* uncoupling_adresses = new size_t[adress];
            for (int i = 0; i < adress; i++) {
                std::cout << "adress: \n";
                fill = InputCheck();
                if (fill > train.Train_Size()) {
                    std::cout << "Incorrect input! No such carriage!\n";
                    return;
                }
                uncoupling_adresses[i] = fill ;
            }
            Train new_train = train.Uncoupling_Train(adress, uncoupling_adresses);
            std::cout << "Uncoupled train (new):\n";
            std::cout << new_train;
            std::cout << "Uncoupled train (old):\n";
            std::cout << train;
            delete uncoupling_adresses;
        }

        if (choice == 5) {
            std::cout << train;
        }

        if (choice == 6) {
            std::cout << "Total amount of people:" << train.People_in_Train() << "\n";
        }

        if (choice == 7) {
            std::cout << "Carriage adress: ";
            adress = InputCheck();
            if (adress > train.Train_Size()) {
                std::cout << "Incorrect input! No such carriage!\n";
                return;
            }
            std::cout << train.Carriage_Fill(adress) <<"\n";
        }

        if (choice == 0) {
            break;
        }
    }
}
