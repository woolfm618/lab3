#include <iostream>
#include <stdlib.h>
#include <vector>
#include <exception>



struct Carriage {                                // ����� 
	size_t capacity;                                // ���� ����� ���� 
	size_t fill;									 // ������ ���� 
};

class TrainError: public std::exception {};         
class TooManyPeopleError : public TrainError {};
class IndexError : public TrainError {};
class DeleteError : public TrainError {};

class Train {													  // �����

    friend std::istream& operator>>(std::istream& stream, Train& train);
	friend std::ostream& operator<<(std::ostream& stream, const Train& train);

private:
	std::vector <Carriage> _body;
	size_t allpeople;          

public:

	Train();                                          // ������������� ��� �������						 

	void Create_Carriage(size_t fill, size_t size_carriage);                              // �������� ����� � �����

	void Fill_Carriage(size_t adress, size_t seats_amount);            // ��������� ������ � ������

	size_t Carriage_Fill(size_t adress);                        // ����� ����� � ������

	Train Uncoupling_Train(size_t amount_uncoupling_carriages, size_t* carriages_adress); // �������� (����� ������������, ������)

	size_t People_in_Train();										 // ����� ����� ����� � ������

	size_t Train_Size();

	void Delete_Carriage(int adress);

	//size_t Find_Carriage(size_t fill, size_t size_carriage);

	Train& operator+=(const Train& other );
};

std::istream& operator>>(std::istream& stream, Train& train);

std::ostream& operator<<(std::ostream& stream, const Train& train); // ����� 

Train operator+(const Train& first, const Train& second);   

Train operator*(const Train& first, size_t number); // ��������� 